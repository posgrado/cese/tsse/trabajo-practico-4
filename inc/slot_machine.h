/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/10
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _SLOT_MACHINE_H_
#define _SLOT_MACHINE_H_

/*=====[Inclusions of public function dependencies]==========================*/

#include "FreeRTOS.h"
#include "queue.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

void init_slot_task(xQueueHandle *long_poll_queue,
		xQueueHandle *long_poll_ans_queue);

void slot_task(void * pvParameters);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

void on_slot_rx(void *noUsado);
void on_slot_tx(void *noUsado);

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _SLOT_MACHINE_H_ */

