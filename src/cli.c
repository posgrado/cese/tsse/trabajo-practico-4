/*=====cli================================================================*/

/*=========================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/09
 *========================================================================*/

/*=====[Inclusion of own header]=========================================*/

#include "cli.h"

/*=====[Inclusions of private function dependencies]=====================*/

#include "button.h"
#include "error_handle.h"
#include "sapi.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "string.h"

/*=====[Definition macros of private constants]==========================*/

/*=====[Private function-like macros]====================================*/

/*=====[Definitions of private data types]===============================*/

/*=====[Definitions of external public global variables]=================*/

/*=====[Definitions of public global variables]==========================*/

/*=====[Definitions of private global variables]=========================*/

static xQueueHandle cli_read_queue;

static xQueueHandle cli_write_queue;

static const uint8_t *prompt = "\r\nsas_cli>";

static xQueueHandle *long_poll_req_queue_ptr;

static xQueueHandle *long_poll_ans_queue_ptr;

/*=====[Prototypes (declarations) of external public functions]==========*/

/*=====[Prototypes (declarations) of private functions]==================*/

static void processCommand(uint8_t * DataReceived, uint8_t length);
static void cli_tx_string(const uint8_t *str);
static void cli_tx_byte(uint8_t c);
static void process_slot_response(uint8_t command_code, uint8_t command_length);


/*=====[Implementations of public functions]=============================*/

void cli_init(xQueueHandle *long_poll_queue,
xQueueHandle *long_poll_ans_queue) {
	/* Inicializar la UART_USB junto con las interrupciones de Tx y Rx */
	uartConfig(UART_USB, 115200);
	// Seteo un callback al evento de recepcion y habilito su interrupcion
	uartCallbackSet(UART_USB, UART_RECEIVE, on_cli_rx, NULL);
	// Seteo un callback al evento de recepcion y habilito su interrupcion
	uartCallbackSet(UART_USB, UART_TRANSMITER_FREE, on_cli_tx, NULL);
	// Habilito todas las interrupciones de UART_USB
	uartInterrupt(UART_USB, TRUE);
	cli_read_queue = xQueueCreate(100, sizeof(uint8_t));
	if (pdFAIL != cli_read_queue) {
		cli_write_queue = xQueueCreate(100, sizeof(uint8_t));
	} else {
		error();
	}
	if (pdFAIL != cli_write_queue) {
		long_poll_req_queue_ptr = long_poll_queue;
		long_poll_ans_queue_ptr = long_poll_ans_queue;
	} else {
		error();
	}
}

void cli_task() {
	uint8_t ByteReceived;
	uint8_t CommandReceived[100];
	uint8_t length = 0;

//	volatile uint32_t cyclesElapsed = 0;
//	// Resetea el contador de ciclos
//	cyclesCounterReset();
//	// Funcion bloqueante: 142040
//	uartWriteString(UART_USB, prompt);
//	// Funcion por interrupcion: 5692
	cli_tx_string(prompt);
//	// Guarda en una variable los ciclos leidos
//	cyclesElapsed = cyclesCounterRead();

	while (TRUE) {
		xQueueReceive(cli_read_queue, &ByteReceived, portMAX_DELAY);

		//echo
		cli_tx_byte(ByteReceived);
		CommandReceived[length] = ByteReceived;
		length++;

		if ('\r' == ByteReceived) {
			//Agrega \n 0x00 para completar un string
			CommandReceived[length++] = '\n';
			CommandReceived[length] = 0x00;
			processCommand(CommandReceived, length);
			cli_tx_string(prompt);
			length = 0;
		}
	}
}

/*=====[Implementations of interrupt functions]==============================*/

void on_cli_rx(void *noUsado) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	uint8_t c = uartRxRead(UART_USB);

	if (errQUEUE_FULL
			!= xQueueSendFromISR(cli_read_queue, &c, &xHigherPriorityTaskWoken)) {
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	} else {
		error();
	}
}

void on_cli_tx(void *noUsado) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	uint8_t c;

	if (xQueueReceiveFromISR(cli_write_queue, &c, &xHigherPriorityTaskWoken)) {
		uartTxWrite(UART_USB, c);
	} else {
		uartCallbackClr(UART_USB, UART_TRANSMITER_FREE);
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/*=====[Implementations of private functions]================================*/

static void processCommand(uint8_t * data_received, uint8_t length) {
//Lista de comandos
	const uint8_t *cmd_list[] = { "?\r\n",	//Comando 0 = help
			"pollrate\r\n",  				//Comando 1
			"eventlist\r\n", 				//Comando 2
			"currentcredit\r\n",  			//Comando 3
			"meters\r\n",  					//Comando 4
			"machineid\r\n"					//Comando 5
			};
	uint8_t qty_cmds = 6;

//Imprimo nueva linea para la salida
	cli_tx_string("\r\n");

	if (0 == strcmp(data_received, cmd_list[0])) {
		//Comando 0 = ? (help)
		for (int i = 0; i < qty_cmds; ++i) {
			cli_tx_string(cmd_list[i]);
		}
	} else if (0 == strcmp(data_received, cmd_list[1])) {
		//Comando 1 = pollrate
		uint8_t resultado[3];
		itoa(button_get_poll_rate(), resultado, 10);
		cli_tx_byte_array(resultado, 3);
		cli_tx_string(" ms");
	} else if (0 == strcmp(data_received, cmd_list[2])) {
		//Comando 2 = eventlist
		cli_tx_string("Not implemented yet!");
	} else if (0 == strcmp(data_received, cmd_list[3])) {
		//Comando 3 = currentcredit
		process_slot_response(0x1A, 8);
	} else if (0 == strcmp(data_received, cmd_list[4])) {
		//Comando 4 = meters
		process_slot_response(0x1C, 36);
	} else if (0 == strcmp(data_received, cmd_list[5])) {
		//Comando 5 = machineid
		process_slot_response(0x1F, 24);
	} else if (0 == strcmp(data_received, "\r\n")) {
		//Comando vacio
	} else {
		//Comando no encontrado = Command Not Found
		cli_tx_string("Command Not Found: ");
		cli_tx_byte_array(data_received, length);
	}
}

static void cli_tx_string(const uint8_t *str) {
	cli_tx_byte_array(str, 100);
}

static void cli_tx_byte(uint8_t c) {
	cli_tx_byte_array(&c, 1);
}

void cli_tx_byte_array(const uint8_t* byte_array, uint32_t byteArrayLen) {
	uint8_t c = '\0';
	uint8_t i = 0;
	do {
		c = byte_array[i++];
		if (errQUEUE_FULL == xQueueSend(cli_write_queue, &c, 0)) {
			error();
		}
	} while (c != '\0' && i < byteArrayLen);
	uartCallbackSet(UART_USB, UART_TRANSMITER_FREE, on_cli_tx, NULL);
	uartSetPendingInterrupt(UART_USB);
}

static void process_slot_response(uint8_t command_code, uint8_t command_length) {
	uint8_t command_response[100];
	uint8_t i;
	char str[100];
	if (pdTRUE == xQueueSend(*long_poll_req_queue_ptr, &command_code, 0)) {
		cli_tx_string("Waiting response fron slot machine...\n\r");
		uint8_t message;
		i = 0;
		while (command_length > i) {
			xQueueReceive(*long_poll_ans_queue_ptr, &message, portMAX_DELAY);
			command_response[i] = message;
			i++;
		}
		switch (command_code) {
		case 0x1A:
			for (int j = 2; j < 6; ++j) {
				itoa(command_response[j], str, 16);
				cli_tx_string(str);
			}
			break;
		case 0x1C:
			for (int j = 2; j < 34; ++j) {
				itoa(command_response[j], str, 16);
				cli_tx_string(str);
			}
			break;
		case 0x1F:
			for (int j = 2; j < 22; ++j) {
				itoa(command_response[j], str, 16);
				cli_tx_string(str);
			}
			break;
		}
	} else {
		cli_tx_string("It was impossible to queue the request...");
	}
}

/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukas Chmela
 * Released under GPLv3.

 */
char* itoa(int value, char* result, int base) {
	// check that the base if valid
	if (base < 2 || base > 36) {
		*result = '\0';
		return result;
	}

	char* ptr = result, *ptr1 = result, tmp_char;
	int tmp_value;

	do {
		tmp_value = value;
		value /= base;
		*ptr++ =
				"zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35
						+ (tmp_value - value * base)];
	} while (value);

	// Apply negative sign
	if (tmp_value < 0)
		*ptr++ = '-';
	*ptr-- = '\0';
	while (ptr1 < ptr) {
		tmp_char = *ptr;
		*ptr-- = *ptr1;
		*ptr1++ = tmp_char;
	}
	return result;
}
