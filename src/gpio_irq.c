/*=====gpio_irq===========================================================*/

/*=========================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/10
 *========================================================================*/

/*=====[Inclusion of own header]==========================================*/

#include "gpio_irq.h"
#include "sapi.h"

/*=====[Inclusions of private function dependencies]======================*/

/*=====[Definition macros of private constants]===========================*/
#ifndef GPIO_INT_PRIORITY
#define GPIO_INT_PRIORITY	7
#endif

/*=====[Private function-like macros]=====================================*/

/*=====[Definitions of private data types]================================*/

/*=====[Definitions of external public global variables]==================*/

extern const pinInitGpioLpc4337_t gpioPinsInit[];

/*=====[Definitions of public global variables]===========================*/

/*=====[Definitions of private global variables]==========================*/

static irqChannels_t irqChannels[7];
static uint8_t irqChannel;

/*=====[Prototypes (declarations) of external public functions]===========*/

/*=====[Prototypes (declarations) of private functions]===================*/

static void gpioProcessIRQ(uint8_t irqChannel);

static void gpioClearInterrupt(uint8_t irqChannel);

/*=====[Implementations of public functions]==============================*/

void gpioCallbackSet(gpioMap_t gpio, gpioEvents_t event,
		callBackFuncPtr_t callbackFunc, void* callbackParam) {
	if (callbackFunc != 0 && irqChannel < 7) {
		// Set callback
		irqChannels[irqChannel].isrCallbackGPIO = callbackFunc;
		irqChannels[irqChannel].isrCallbackGPIOParams = callbackParam;
		irqChannels[irqChannel].event = event;
		irqChannels[irqChannel].gpio = gpio;

		//(Channel 0 to 7, GPIO Port, GPIO Pin)
		Chip_SCU_GPIOIntPinSel(irqChannel, gpioPinsInit[gpio].gpio.port,
				gpioPinsInit[gpio].gpio.pin);

		if (INT_FALL == event || INT_RISE == event) {
			//Edge mode
			Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH(irqChannel));
		} else if (INT_LOW == event || INT_HIGH) {
			//Level mode
			Chip_PININT_SetPinModeLevel(LPC_GPIO_PIN_INT, PININTCH(irqChannel));
		}
		if (INT_FALL == event || INT_LOW == event) {
			//Fall edge or Low Level
			Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH(irqChannel));
		} else if (INT_RISE == event || INT_HIGH == event) {
			//Rise edge or High Level
			Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH(irqChannel));
		}
		irqChannel++;
	}
}

void gpioInterrupt(gpioMap_t gpio, bool_t enable) {
#ifdef USE_FREERTOS
#if GPIO_INT_PRIORITY < configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY
#error Priority must be equal or greater than configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY
#endif
#endif
	for (uint8_t i = 0; i < 7; ++i) {
		if (irqChannels[i].gpio == gpio) {
			if (enable) {
				NVIC_SetPriority(PIN_INT0_IRQn + i, GPIO_INT_PRIORITY);
				NVIC_EnableIRQ(PIN_INT0_IRQn + i);
			} else {
				NVIC_DisableIRQ(PIN_INT0_IRQn + i);
			}
		}
	}
}
/*=====[Implementations of interrupt functions]===========================*/

void GPIO0_IRQHandler(void) {
	gpioProcessIRQ(0);
}

void GPIO1_IRQHandler(void) {
	gpioProcessIRQ(1);
}

void GPIO2_IRQHandler(void) {
	gpioProcessIRQ(2);
}

void GPIO3_IRQHandler(void) {
	gpioProcessIRQ(3);
}

void GPIO4_IRQHandler(void) {
	gpioProcessIRQ(4);
}

void GPIO5_IRQHandler(void) {
	gpioProcessIRQ(5);
}

void GPIO6_IRQHandler(void) {
	gpioProcessIRQ(6);
}

void GPIO7_IRQHandler(void) {
	gpioProcessIRQ(7);
}

/*=====[Implementations of private functions]================================*/

static void gpioProcessIRQ(uint8_t irqChannel) {
	switch (irqChannels[irqChannel].event) {
	case INT_FALL:
		if (Chip_PININT_GetFallStates(LPC_GPIO_PIN_INT)
				& PININTCH(irqChannel)) {
			// Execute fall callback
			if (irqChannels[irqChannel].isrCallbackGPIO != 0) {
				gpioClearInterrupt(irqChannel);
				(*irqChannels[irqChannel].isrCallbackGPIO)(
						irqChannels[irqChannel].isrCallbackGPIOParams);
			}
		}
		break;
	case INT_RISE:
		if (Chip_PININT_GetRiseStates(LPC_GPIO_PIN_INT)
				& PININTCH(irqChannel)) {
			// Execute rise callback
			if (irqChannels[irqChannel].isrCallbackGPIO != 0) {
				gpioClearInterrupt(irqChannel);
				(*irqChannels[irqChannel].isrCallbackGPIO)(
						irqChannels[irqChannel].isrCallbackGPIOParams);
			}
		}
		break;
	case INT_LOW:
		break;
	case INT_HIGH:
		break;
	default:
		break;
	}
}

static void gpioClearInterrupt(uint8_t irqChannel) {
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH(irqChannel));
}
