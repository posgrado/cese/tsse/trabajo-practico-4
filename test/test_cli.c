#include "unity.h"

#include "cli.h"
#include "mock_sapi_uart.h"
#include "mock_queue.h"
#include "mock_button.h"


BaseType_t xQueueSendStub( QueueHandle_t xQueue, const void * const pvItemToQueue, TickType_t xTicksToWait, const BaseType_t xCopyPosition,  int NumCalls){
    printf("Llamada %d, valor: %c\r\n", NumCalls, *(char*) pvItemToQueue);
    switch(NumCalls){
        case 0:
            TEST_ASSERT_EQUAL_INT('P', *(char*) pvItemToQueue);
            break;
        case 1:
            TEST_ASSERT_EQUAL_INT('r', *(char*) pvItemToQueue);
            break;
        case 2:
            TEST_ASSERT_EQUAL_INT('u', *(char*) pvItemToQueue);
            break;
        case 3:
            TEST_ASSERT_EQUAL_INT('e', *(char*) pvItemToQueue);
            break;
        case 4:
            TEST_ASSERT_EQUAL_INT('b', *(char*) pvItemToQueue);
            break;
        case 5:
            TEST_ASSERT_EQUAL_INT('a', *(char*) pvItemToQueue);
            break;
        case 6:
            TEST_ASSERT_EQUAL_INT('\0', *(char*) pvItemToQueue);
            break;
        default:
            TEST_FAIL_MESSAGE("Numero de llamada fuera del rango");
    }
    return 1;
}

void setUp(void)
{
}

void test_itoa_base10(){
    char buf[11];
    itoa(2147483647, buf, 10);
    TEST_ASSERT_EQUAL_STRING("2147483647", buf);
    itoa(-2147483648, buf, 10);
    TEST_ASSERT_EQUAL_STRING("-2147483648", buf);
}

void test_itoa_base16(){
    char buf[11];
    itoa(0xFF, buf, 16);
    TEST_ASSERT_EQUAL_STRING("ff", buf);
    itoa(0x7FFFFFFF, buf, 16);
    TEST_ASSERT_EQUAL_STRING("7fffffff", buf);
    itoa(0x00000000, buf, 16);
    TEST_ASSERT_EQUAL_STRING("0", buf);
}

void test_cli_tx_byte_array(){
    xQueueHandle cli_write_queue;
    uartInit_Ignore();
    uartCallbackSet_Ignore();
    uartInterrupt_Ignore();
    xQueueGenericCreate_IgnoreAndReturn(cli_write_queue);

    cli_init(NULL, NULL);

    const uint8_t byte_array[] = "Prueba";

    xQueueGenericSend_Stub(xQueueSendStub);
    uartSetPendingInterrupt_Ignore();

    uint32_t byteArrayLen = sizeof(byte_array);
    cli_tx_byte_array(byte_array, byteArrayLen);
}
