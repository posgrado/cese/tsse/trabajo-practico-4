
#ifndef _SAPI_UART_H_
#define _SAPI_UART_H_

#include "sapi_peripheral_map.h"
#include "sapi_datatypes.h"

#define uartConfig uartInit

typedef void (*callBackFuncPtr_t)(void *);

typedef enum{
   UART_RECEIVE,
   UART_TRANSMITER_FREE
} uartEvents_t;

void uartInit( uartMap_t uart, uint32_t baudRate );
void uartCallbackSet( uartMap_t uart, uartEvents_t event,
                      callBackFuncPtr_t callbackFunc, void* callbackParam );
void uartCallbackClr( uartMap_t uart, uartEvents_t event );
void uartSetPendingInterrupt(uartMap_t uart);
void uartInterrupt( uartMap_t uart, bool_t enable );
uint8_t uartRxRead( uartMap_t uart );
void uartTxWrite( uartMap_t uart, uint8_t value );

#endif
