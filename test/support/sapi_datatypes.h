/*==================[typedef]================================================*/


#ifndef _SAPI_DATATYPES_H_
#define _SAPI_DATATYPES_H_
#include "stdint.h"

/* Functional states */
#ifndef ON
#define ON     1
#endif
#ifndef OFF
#define OFF    0
#endif

#ifndef FALSE
#define FALSE  0
#endif
#ifndef TRUE
#define TRUE   (!FALSE)
#endif
/* Define Boolean Data Type */
typedef uint8_t bool_t;

#endif
