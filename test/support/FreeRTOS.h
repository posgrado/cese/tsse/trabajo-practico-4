
#ifndef FREERTOS_H
#define FREERTOS_H


typedef long BaseType_t;

#define xQueueHandle QueueHandle_t
#define pdFAIL			( pdFALSE )
#define pdFALSE			( ( BaseType_t ) 0 )
#define pdTRUE			( ( BaseType_t ) 1 )
#define portYIELD()
#define portEND_SWITCHING_ISR( xSwitchRequired ) if( xSwitchRequired != pdFALSE ) portYIELD()
#define portYIELD_FROM_ISR( x ) portEND_SWITCHING_ISR( x )

#endif
