# FreeRTOS Sandbox
This project was created to play making a new template to use  EDU-CIAA-NXP board, FreeRTOS, Unity test, sonarcloud.io, GitLab CI, Doxygen and a bunch of new tools learned during the Embedded Systems Specialization of University of Buenos Aires.

[TOC]

## Getting Started
This project is exported after changing. You can import it in your GitLab account in order to have a new project from this template.

### Prerequisites
- https://gitlab.com account ([sign up now!](https://gitlab.com/users/sign_up))
- You'll need a [GitLab Personal Access Token](https://gitlab.com/profile/personal_access_tokens), use **SonarCloud** as name, select **api** scope and create one.
- Copy that token in your clipboard.
- https://sonarcloud.io account ([sign up using your GitLab account](https://sonarcloud.io/sessions/init/gitlab))
- Click **Authorize**
- Click **Import projects from GitLab**
- Click **Import my personal GitLab group**
- Paste your GitLab Personal Access Token and click **Continue**
- You may change your organization key (I left it as default) and click **Continue**
- Choose a plan (**Free** in my case 😉) and **Create Organization**
- You're ready to use GitLab.com and SonarCloud.io

### New GitLab project from this template
1. [Download](https://gitlab.com/davidbro-in/freertos-sandbox/download_export) this exported project
1. Go to [import project](https://gitlab.com/projects/new#import_project) and select **GitLab export**
1. Write a **Project name** and **Choose File** under GitLab project export section
1. Click **Import Project** and you're done!

### Add project to sonarcloud.io
1. Go to https://sonarcloud.io/projects/create and select the project you've recently created
1. Click **SetUp**
1. Click the huge GitLab logo (**With GitLab CI/CD Pipeline**)
1. Follow the given instructions to  define **only** the `SONAR_TOKEN` variable (`SONAR_HOST_URL` is not necessary!)
1. That's all you need to do in sonarcloud.io
1. In your GitLab project, run a new Pipeline. Go to **CI/CD**, press the **Run Pipeline** button and then press the new **Run Pipeline** button.
1. Go to https://sonarcloud.io/projects and your new project is there.
1. In your GitLab project, fix your badges. Go to **Settings > General > Badges** and edit yout Quality Gate and Coverage badges. You can find your sonarclould.io badges in the project's page (bottom-rigth corner)

## Contact

If you want to contact me you can reach me at davidmbroin@gmail.com.

## License

This project uses the following license: [MIT](LICENSE.txt).
